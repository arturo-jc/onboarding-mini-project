# Onboarding Mini Project
## Install server dependencies
``` 
$ cd server/
$ npm i
```
## Start server
``` 
$ npm start
```
## Install client depedencies
``` 
$ cd client/
$ npm i
```
## Start client
``` 
$ ng serve
```