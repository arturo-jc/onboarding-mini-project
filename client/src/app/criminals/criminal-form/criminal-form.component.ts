import { Component, OnDestroy, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Criminal } from "src/app/types";
import { Validators } from '@angular/forms';
import { CriminalService } from "src/app/criminal.service";
import { MessageService } from "primeng-lts/api";
import { Subscription, pipe } from "rxjs";
import { map } from "rxjs/operators";
import { CriminalFormService } from "src/app/criminal-form.service";
import { ActivatedRoute, Router } from "@angular/router";

interface WantedOption {
    label: string,
    value: boolean
}

@Component({
    selector: 'app-criminal-form',
    templateUrl: './criminal-form.component.html'
})
export class CriminalFormComponent implements OnInit, OnDestroy {
    criminal: Criminal;
    criminalForm: any;
    wantedOptions: WantedOption[];
    selectedAvatar: String;
    avatarOptions: String[];
    avatarSubscription: Subscription;
    criminalSubscription: Subscription;
    dataSaved: boolean;
    unlistenFuncs = [];
    @ViewChild('criminalFormRef') criminalFormRef: ElementRef;

    constructor(
        private fb: FormBuilder,
        private criminalService: CriminalService,
        private criminalFormService: CriminalFormService,
        private messageService: MessageService,
        private route: ActivatedRoute,
        private router: Router
    ){}

    ngOnInit(): void {
        this.dataSaved = false;

        this.criminalService.criminalEmitter.subscribe(criminal => {
            this.criminal = criminal;
            this.selectedAvatar = this.criminal.avatar;
            this.initForm();
            this.avatarSubscription = this.criminalService.getAllAvatars()
            .valueChanges
            .pipe(
                map(result => result.data.criminals.criminals)
            )
            .subscribe(criminals => {
                this.avatarOptions = criminals.map(criminal => criminal.avatar);
            });
        })
    }



    initForm(): void {
        this.wantedOptions = [
            {
                label: 'Alive',
                value: true
            },
            {
                label: 'Dead or alive',
                value: false
            }
        ]
        
        this.criminalForm = this.fb.group({
            avatar: [this.criminal.avatar],
            firstName: [this.criminal.first_name, Validators.required],
            lastName: [this.criminal.last_name, Validators.required],
            // convert dob from unix timestamp to date
            dateOfBirth: [new Date(+this.criminal.date_of_birth * 1000)],
            occupation: [this.criminal.occupation, Validators.required],
            gender: [this.criminal.gender, Validators.required],
            email: [this.criminal.email, Validators.required],
            reward: [+this.criminal.reward],
            contractIsOpen: [this.criminal.contract_is_open],
            mustReturnAlive: [this.criminal.must_return_alive],
            city: [this.criminal.address.city, Validators.required],
            streetName: [this.criminal.address.street_name, Validators.required],
            streetAddress: [this.criminal.address.street_address, Validators.required],
            zipCode: [this.criminal.address.zip_code, Validators.required],
            state: [this.criminal.address.state, Validators.required],
            country: [this.criminal.address.country, Validators.required]
        });
    }

    onSelectAvatar(): void {
        this.selectedAvatar = this.criminalForm.value.avatar;
        this.criminalFormService.avatarChange.next(this.criminalForm.value.avatar);
    }

    onSave(): void {

        const {
            avatar,
            firstName,
            lastName,
            dateOfBirth,
            occupation,
            gender,
            email,
            reward,
            contractIsOpen,
            mustReturnAlive,
            city,
            streetName,
            streetAddress,
            zipCode,
            state,
            country
        } = this.criminalForm.value;

        const updates = {
            avatar,
            first_name: firstName,
            last_name: lastName,
            // convert dob back to unix timestamp
            date_of_birth: dateOfBirth.getTime() / 1000,
            occupation,
            gender,
            email,
            reward,
            contract_is_open: contractIsOpen,
            must_return_alive: mustReturnAlive,
            city,
            street_name: streetName,
            street_address: streetAddress,
            zip_code: zipCode,
            state,
            country
        };
        
        this.criminalService.updateCriminal(this.criminal.id, updates)
        .subscribe(() => {
            this.dataSaved = true;
            this.messageService.add({severity: 'success', summary: 'Criminal updated', detail: `The records for ${firstName} ${lastName} were updated.`});
            this.router.navigate(['../info'], {relativeTo: this.route});
        });

    }

    validateAndScroll(): void {
        const firstInvalidControlName = Object.keys(this.criminalForm.controls).find(
            key => this.criminalForm.controls[key].errors
        );
        if(firstInvalidControlName){
            const inputSelector = `[formControlName=${firstInvalidControlName}]`;
            const firstInvalidInput = document.querySelector(inputSelector);

            // Scroll to 20 px above invalid input
            const inputPosition = firstInvalidInput.getBoundingClientRect().top;
            const offset = 20;
            window.scrollTo({
                top: inputPosition + window.pageYOffset - offset,
                behavior: 'smooth'
            });

            // Convert field name from camel case to regular string
            const fieldName = firstInvalidControlName
            .replace(/([A-Z])/g, ' $1')
            .toLowerCase()
            .replace(/^./, function(str){ return str.toUpperCase(); });

            this.messageService.add({severity: 'error', summary: 'Invalid field', detail: `${fieldName} is required!`});
        }
    };

    ngOnDestroy(): void {
        this.avatarSubscription.unsubscribe();
        this.unlistenFuncs.forEach(unlisten => unlisten());
        }
}