import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Criminal } from "src/app/types";
import { CriminalService } from "src/app/criminal.service";
import { ConfirmationService, MessageService } from "primeng-lts/api";


@Component({
    selector: 'app-criminal-toolbar',
    templateUrl: './criminal-toolbar.component.html'
})
export class CriminalToolbarComponent {
    @Input('criminal') criminal: Criminal;

    constructor(
        private criminalService: CriminalService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private router: Router
    ){}

    closeContract(): void {
        this.criminalService.closeContract(this.criminal.id)
        .subscribe(result => {
            this.criminal.contract_is_open = result.data.closeContract.contract_is_open;
            this.router.navigate(['/criminals']);
            this.messageService.add({severity: 'success', summary: 'Contract closed', detail: `The contract for ${this.criminal.first_name} ${this.criminal.last_name} was closed.`})
        });
    }

    deleteCriminal(): void {
        this.criminalService.deleteCriminal(this.criminal.id)
        .subscribe(() => {
            this.router.navigate(['/criminals']);
            this.messageService.add({severity: 'success', summary: 'Criminal deleted', detail: `${this.criminal.first_name} ${this.criminal.last_name} was deleted from the most wanted list.`})
        });
    }

    confirmDelete(): void {
        this.confirmationService.confirm({
            message: "Are you sure you want to delete this criminal? This action cannot be undone.",
            header: 'Delete criminal',
            accept: this.deleteCriminal.bind(this)
        })
    }

    goBack(): void {
        this.router.navigate(['/criminals'])
    }
}