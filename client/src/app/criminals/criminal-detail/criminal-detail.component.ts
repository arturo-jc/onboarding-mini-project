import { Component, OnDestroy, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { Criminal } from "src/app/types";
import { CriminalService } from "src/app/criminal.service";
import { Subscription } from "rxjs";
import { CriminalFormService } from "src/app/criminal-form.service";
import { MenuItem } from "primeng-lts/api";

@Component({
    selector: 'app-criminal-detail',
    templateUrl: './criminal-detail.component.html'
})
export class CriminalDetailComponent implements OnInit, OnDestroy {

    criminal: Criminal;
    criminalSubscription: Subscription;
    avatarSubscription: Subscription;
    changesSavedSubscription: Subscription;
    formSavedSubscription: Subscription;
    loading: boolean = true;
    avatar: string;
    items: MenuItem[];
    activeItem: MenuItem;

    constructor(
        private criminalService: CriminalService,
        private criminalFormService: CriminalFormService,
        private route: ActivatedRoute,
        private location: Location
        ){}
    
    ngOnInit(): void {

        // Config tab menu
        this.items = [
            {
                label: 'Info',
                icon: 'pi pi-fw pi-id-card',
                routerLink: ['info']
            },
            { 
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                routerLink: ['edit']
            }
        ]

        // Fetch criminal from server
        const id = +this.route.snapshot.paramMap.get('id');
        this.criminalSubscription = this.criminalService.findCriminalById(id)
            .valueChanges
            .subscribe(({data, loading}) => {
                this.loading = loading;
                if(data.criminal){
                    this.criminal = data.criminal;
                    this.criminalService.initCriminalEmitter(this.criminal);
                    this.avatar = data.criminal.avatar;
                }
            });

        // Listen for avatar selection events in form
        this.avatarSubscription = this.criminalFormService.avatarChange.subscribe(avatar => {
            this.avatar = avatar;
        })

    }

    ngOnDestroy(): void {
        this.criminalSubscription.unsubscribe();
        this.avatarSubscription.unsubscribe();
    }

    getLastUrlSegment(): string {
        let locationPath = this.location.path();
        return locationPath.split('/')[3];
      }
}