import { Component, OnDestroy, OnInit } from "@angular/core";
import { QueryRef } from "apollo-angular";
import { ConfirmationService, LazyLoadEvent, MessageService } from "primeng-lts/api";
import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CriminalService } from "../criminal.service";

import { Criminal, Query } from "../types";

interface Col {
    field: string;
    header: string;
}

@Component({
    selector: 'app-criminals',
    templateUrl: './criminals.component.html',
    styleUrls: ['./criminals.component.scss']
})
export class CriminalsComponent implements OnInit, OnDestroy {

    criminals: Criminal[];
    cols: Col[];
    searchTerm = '';

    // QueryRefs and subs
    criminalQueryRef: QueryRef<Query>;
    searchBoxWatch = new Subject<string>();
    subscriptions: Subscription[] = [];

    // Configure big rewards tag
    bigReward = 9000;

    // Selection functionality
    selectedCriminals: Criminal[];
    selectedCriminal: Criminal;
    selectedOpenContracts: Criminal[];

    // Pagination functionality
    totalCriminals: number;
    criminalsPerPage = 10;
    currentOffset = 0;

    constructor(
        private criminalService: CriminalService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService
    ) { }

    ngOnInit(): void {
        this.cols = [
            { field: '', header: 'Name'},
            { field: 'contract_is_open', header: 'Contract'},
            { field: 'must_return_alive', header: 'Wanted'},
            { field: 'reward', header: 'Reward'}
        ];

        // Watch for changes in search box
        const searchBoxSubscription = this.searchBoxWatch.pipe(
            debounceTime(300),
            distinctUntilChanged()
        )
        .subscribe((text: string) => {
            this.searchTerm = text;
            this.fetchCriminals();
        })

        this.subscriptions.push(searchBoxSubscription);
    }

    ngOnDestroy(): void {
        for(const sub of this.subscriptions){
            sub.unsubscribe();
        }
    }

    /**
    * Updates the current offset and fetches criminals.
    * Runs for the first time when the component is first rendered and then every time the user interacts with the paginator.
    */
    populateTable(event: LazyLoadEvent): void {
        this.currentOffset = event.first;
        this.fetchCriminals();
    }


    /**
    * Refetch Criminal Data, or initialize query if it does not exist
    */
    fetchCriminals(){
        if(!this.criminalQueryRef){
            this.initCriminalQueryRef();
            return;
        }

        this.criminalQueryRef.refetch({
            searchTerm: this.searchTerm,
            offset: this.currentOffset,
            limit: this.criminalsPerPage
        })
    }

    initCriminalQueryRef(){
        this.criminalQueryRef = this.criminalService.findCriminals(this.searchTerm,this.currentOffset,this.criminalsPerPage);
        const criminalsSub = this.criminalQueryRef
        .valueChanges
        .subscribe(({data}) => {
            if(!data){
                return;
            }
            this.criminals = data.criminals.criminals.filter(criminal => !criminal.deleted);
            this.totalCriminals = data.criminals.totalRecords;
        },
        (error) => {
            if(error.graphQLErrors && error.graphQLErrors.length){
                this.messageService.add({severity: 'error', summary: 'Error', detail: error.graphQLErrors[0].message});
                return;
            };
            this.messageService.add({severity: 'error', summary: 'Error', detail: error.message});
        })
        this.subscriptions.push(criminalsSub);
    }

    updateSearchTerm(term: string): void{
        this.searchBoxWatch.next(term);
    }

    onClose(): void {
        const closeContractsSub = this.criminalService.closeManyContracts(this.selectedOpenContracts)
        .subscribe(result => {
            const contractsNum = result.data.closeManyContracts.length;
            this.messageService.add({severity: 'success', summary: 'Contracts closed', detail: `You just closed ${contractsNum} contracts. Look at you!`});
            this.selectedCriminals = [];
            this.selectedOpenContracts = [];
        });

        this.subscriptions.push(closeContractsSub);
    }

    onDelete(): void {
        const deleteSub = this.criminalService.deleteManyCriminals(this.selectedCriminals)
        .subscribe(() => {
            this.messageService.add({severity: 'success', summary: 'Criminals deleted', detail: `You just deleted the records for ${this.selectedCriminals.length} criminals.`});
            if (this.selectedCriminals.includes(this.selectedCriminal)){
                this.selectedCriminal = null;
            }
            this.selectedCriminals = [];
            this.selectedOpenContracts = [];
        })
        this.subscriptions.push(deleteSub);
    }

    confirmDelete(): void {
        this.confirmationService.confirm({
            message: "Are you sure you want to delete these criminals? This action cannot be undone.",
            header: 'Delete criminals',
            accept: this.onDelete.bind(this)
        })
    }

    updateSelection(): void {
        this.selectedOpenContracts = this.selectedCriminals.filter(criminal => criminal.contract_is_open);
    }

    updateSelectedCriminal(event): void {
        this.selectedCriminal = event.data;
        this.updateSelection();
    }

    unselectCriminal(unselectedCriminal): void {
        this.selectedCriminal = null;
        this.selectedCriminals = this.selectedCriminals
        .filter(criminal => criminal.id !== unselectedCriminal.id);
    }

}