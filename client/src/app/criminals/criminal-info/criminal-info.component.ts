import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CriminalService } from "src/app/criminal.service";
import { Criminal } from "src/app/types";

interface Field{
    label: string;
    value: any;
}

@Component({
    selector: 'app-criminal-info',
    templateUrl: './criminal-info.component.html'
})
export class CriminalInfoComponent implements OnInit, OnDestroy {
    criminal: Criminal;
    fields: Field[];
    criminalSubscription: Subscription;

    constructor(private criminalService: CriminalService){}

    ngOnInit(): void {
        this.criminalSubscription = this.criminalService.criminalEmitter.subscribe(criminal => {
            this.criminal = criminal;
            this.populateTable();
        })
    }

    populateTable(): void {
        this.fields = [
            {
                label: "DOB",
                value: this.criminal.date_of_birth
            },
            {
                label: "Gender",
                value: this.criminal.gender
            },
            {
                label: "Email",
                value: this.criminal.email
            },
            {
                label: "Last known address",
                value: this.criminal.address
            },
            {
                label: "Reward",
                value: this.criminal.reward
            },
            {
                label: "Contract",
                value: this.criminal.contract_is_open
            },
            {
                label: "Wanted",
                value: this.criminal.must_return_alive
            },
        ]
    }

    ngOnDestroy(): void {
        this.criminalSubscription.unsubscribe();
    }
}