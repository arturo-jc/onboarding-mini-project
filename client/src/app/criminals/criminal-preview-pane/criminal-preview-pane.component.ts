import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Criminal } from "src/app/types";

@Component({
    selector: 'app-criminal-preview-pane',
    templateUrl: './criminal-preview-pane.component.html',
})
export class CriminalPreviewPaneComponent{
    @Input('criminal') criminal: Criminal;
    @Output('criminalUnselected') criminalUnselected = new EventEmitter<Criminal>();

    onUnselect(){
        this.criminalUnselected.emit(this.criminal);
    }
}