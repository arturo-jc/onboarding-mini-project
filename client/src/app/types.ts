export type Query = {
    criminals: CriminalQueryResults;
    criminal: Criminal;
    allAvatars: String[];
}

export type Mutation = {
    closeContract: Criminal;
    closeManyContracts: Criminal[];
    updateCriminal: Criminal;
    deleteCriminal: Criminal;
    deleteManyCriminals: Criminal[];
}

export interface Criminal {
    avatar: string;
    id: number;
    first_name: string;
    last_name: string;
    date_of_birth: string;
    gender: string;
    occupation: string;
    email: string;
    address: Address;
    reward: string;
    contract_is_open: boolean;
    must_return_alive: boolean;
    deleted: boolean;
}

export interface CriminalQueryResults {
    criminals: Criminal[];
    totalRecords: number;
}

export type Address = {
    city: string;
    street_name: string;
    street_address: string;
    zip_code: string;
    state: string;
    country: string;
    coordinates: Coordinates;
}

export type Coordinates = {
    lat: number;
    lng: number;
}

export type Update = {
    id?: number;
    avatar: string;
    first_name: string;
    last_name: string;
    date_of_birth: number;
    gender: string;
    occupation: string;
    email: string;
    reward: number;
    contract_is_open: boolean;
    must_return_alive: boolean;
    city: string;
    street_name: string;
    street_address: string;
    zip_code: string;
    state: string;
    country: string;
}