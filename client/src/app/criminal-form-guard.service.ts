import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { CriminalFormComponent } from "./criminals/criminal-form/criminal-form.component";
import { ConfirmationService } from "primeng-lts/api";

@Injectable({
    providedIn: 'root'
})
export class FormGuardService implements CanDeactivate<CriminalFormComponent>{

    constructor(
        private confirmationService: ConfirmationService
    ){}

    canDeactivate(component: CriminalFormComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if(component.criminalForm.dirty && !component.dataSaved){
            return new Promise((resolve, reject) => {
                this.confirmationService.confirm({
                    message: "You have unsaved data. Are you sure you want to navigate away?",
                    header: 'Unsaved data',
                    accept: () => resolve(true),
                    reject: () => resolve(false)
                })
            })
        }
        return true;
    }

    onAccept(){
        return true;
    }
}