import { TestBed } from '@angular/core/testing';

import { CriminalService } from './criminal.service';

describe('CriminalService', () => {
  let service: CriminalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CriminalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
