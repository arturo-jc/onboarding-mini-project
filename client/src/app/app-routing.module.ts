import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CriminalsComponent } from './criminals/criminals.component';
import { CriminalDetailComponent } from './criminals/criminal-detail/criminal-detail.component';
import { CriminalInfoComponent } from './criminals/criminal-info/criminal-info.component';
import { CriminalFormComponent } from './criminals/criminal-form/criminal-form.component';
import { FormGuardService } from './criminal-form-guard.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'criminals'
  },
  {
    path: 'criminals',
    component: CriminalsComponent
  },
  { 
    path: 'criminals/:id',
    component: CriminalDetailComponent,
    children: [
      {
        path: 'info',
        component: CriminalInfoComponent
      },
      {
        path: 'edit',
        canDeactivate: [FormGuardService],
        component: CriminalFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
