import { Injectable } from '@angular/core';
import { Apollo, QueryRef } from "apollo-angular";
import { FetchResult } from 'apollo-link';
import gql from 'graphql-tag';
import { BehaviorSubject, Observable } from 'rxjs';
import { Query, Mutation, Criminal, Update } from './types';

const FIND_CRIMINALS_BY_NAME = gql`
query findCriminals($searchTerm:String, $offset: Int, $limit:Int){
  criminals(searchTerm: $searchTerm, offset: $offset, limit: $limit){
    criminals{
    id
    first_name
    last_name
    occupation
    avatar
    reward
    contract_is_open
    must_return_alive
    deleted
    }
    totalRecords
  }
}`

const FIND_CRIMINAL_BY_ID = gql`
query findCriminal($id: Int!) {
  criminal(id: $id) {
    id
    first_name
    last_name
    date_of_birth
    gender
    occupation
    email
    address{
      city
      street_name
      street_address
      zip_code
      state
      country
      coordinates{
        lat
        lng
      }
    }
    avatar
    reward
    contract_is_open
    must_return_alive
  }
}`

const CLOSE_CONTRACT_BY_CRIMINAL_ID = gql`
mutation closeContract($criminalId: Int!) {
  closeContract(criminalId: $criminalId) {
    id
    contract_is_open
  }
}
`

const CLOSE_MANY_CONTRACTS = gql`
mutation closeContracts($criminalIds: criminalIds) {
  closeManyContracts(criminalIds: $criminalIds){
    id
    contract_is_open
  }
}
`

const UPDATE_CRIMINAL_BY_ID = gql`
mutation updateCriminal($id: Int!, $updates: Update) {
  updateCriminal(id: $id, updates: $updates) {
    id
    avatar
    first_name
    last_name
    date_of_birth
    gender
    occupation
    email
    reward
    contract_is_open
    must_return_alive
    address {
      city
      street_address
      zip_code
      state
      country
    }
  }
}
`

const DELETE_CRIMINAL_BY_ID = gql`
mutation delete($id: Int!) {
  deleteCriminal(id: $id) {
    id
    deleted
  }
}
`

const DELETE_MANY_CRIMINALS = gql`
mutation deleteCriminals($criminalIds: criminalIds) {
  deleteCriminals(criminalIds: $criminalIds){
      id
      first_name
      last_name
      date_of_birth
      avatar
      reward
      contract_is_open
      must_return_alive
      deleted
  }
}`

const GET_ALL_AVATARS = gql`
query getAllAvatars($searchTerm:String = "", $offset: Int = 0, $limit:Int = -1){
  criminals(searchTerm: $searchTerm, offset: $offset, limit: $limit){
    criminals{
    avatar
  	}
	}
}
`

@Injectable({
  providedIn: 'root'
})
export class CriminalService {
  criminalEmitter: BehaviorSubject<Criminal>;

  constructor(private apollo: Apollo) { }

  initCriminalEmitter(criminal: Criminal): void {
    this.criminalEmitter = new BehaviorSubject<Criminal>(criminal);
  }

  findCriminals(searchTerm: string, offset: number, limit: number): QueryRef<Query>{
    return this.apollo.watchQuery<Query>({
      query: FIND_CRIMINALS_BY_NAME,
      variables: {searchTerm, offset, limit},
      fetchPolicy: 'cache-and-network'
    })
  }

  findCriminalById(id: number): QueryRef<Query>{
    return this.apollo.watchQuery<Query>({
      query: FIND_CRIMINAL_BY_ID,
      variables: {id}
  })
  }

  closeContract(criminalId: number): Observable<FetchResult> {
    return this.apollo.mutate<Mutation>({
      mutation: CLOSE_CONTRACT_BY_CRIMINAL_ID,
      variables: {criminalId}
    });
  }

  closeManyContracts(criminals: Criminal[]): Observable<FetchResult> {
    const ids = criminals.map(criminal => criminal.id);
    return this.apollo.mutate<Mutation>({
      mutation: CLOSE_MANY_CONTRACTS,
      variables: {
        criminalIds: {ids}
      }
    });
  }

  updateCriminal(id: number, updates: Update): Observable<FetchResult> {
    return this.apollo.mutate<Mutation>({
      mutation: UPDATE_CRIMINAL_BY_ID,
      variables: {id, updates}
    })
  }

  deleteCriminal(id: number): Observable<FetchResult> {
    return this.apollo.mutate<Mutation>({
      mutation: DELETE_CRIMINAL_BY_ID,
      variables: {id}
    })
  }

  deleteManyCriminals(criminals: Criminal[]): Observable<FetchResult> {
    const ids = criminals.map(criminal => criminal.id);
    return this.apollo.mutate<Mutation>({
      mutation: DELETE_MANY_CRIMINALS,
      variables: {
        criminalIds: {ids}
      }
    })
  }

  getAllAvatars(): QueryRef<Query>{
    return this.apollo.watchQuery<Query>({
      query: GET_ALL_AVATARS
    })
  }
}