import { Pipe, PipeTransform } from "@angular/core";

/*
* Converts a UNIX timestamp representing a person's
* date of a birth to an integer
* representing the person's age
*/

@Pipe({name: 'age'})
export class AgePipe implements PipeTransform {
    transform(value: number): number {
        const birthYear = new Date(value * 1000).getFullYear();
        const currentYear = new Date().getFullYear();
        return currentYear - birthYear;   
    }
}