import { Directive, EventEmitter, HostListener, Output } from "@angular/core";

@Directive({
    selector: '[onEnter]'
})

export class OnEnterDirective {
    @Output('onEnter') onEnter = new EventEmitter<null>();

    @HostListener('keyup.enter') enter(eventData: Event){
        this.onEnter.emit();
    }
}