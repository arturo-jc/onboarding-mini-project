import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AppRoutingModule } from './app-routing.module';

// primeng imports
import { CardModule } from 'primeng-lts/card';
import { TableModule } from 'primeng-lts/table';
import { PaginatorModule } from 'primeng-lts/paginator';
import { AvatarModule } from 'primeng-lts/avatar';
import { ButtonModule } from 'primeng-lts/button';
import { TagModule } from 'primeng-lts/tag';
import { ConfirmDialogModule } from 'primeng-lts/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng-lts/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'primeng-lts/toast';
import { ToolbarModule } from 'primeng-lts/toolbar';
import { ProgressSpinnerModule } from 'primeng-lts/progressspinner';
import { InputTextModule } from 'primeng-lts/inputtext';
import { InputNumberModule } from 'primeng-lts/inputnumber';
import { InputSwitchModule } from 'primeng-lts/inputswitch';
import { DropdownModule } from 'primeng-lts/dropdown';
import { CalendarModule } from 'primeng-lts/calendar';
import { InplaceModule } from 'primeng-lts/inplace';
import { TabMenuModule } from 'primeng-lts/tabmenu';

// Components
import { AppComponent } from './app.component';
import { CriminalsComponent } from './criminals/criminals.component';
import { CriminalDetailComponent } from './criminals/criminal-detail/criminal-detail.component';
import { CriminalFormComponent } from './criminals/criminal-form/criminal-form.component';
import { CriminalInfoComponent } from './criminals/criminal-info/criminal-info.component';
import { CriminalToolbarComponent } from './criminals/criminal-toolbar/criminal-toolbar.component';
import { CriminalPreviewPaneComponent } from './criminals/criminal-preview-pane/criminal-preview-pane.component';

// Pipes
import { AgePipe } from './age.pipe';
import { AvatarPipe } from './avatar-name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CriminalsComponent,
    CriminalDetailComponent,
    CriminalFormComponent,
    CriminalInfoComponent,
    CriminalToolbarComponent,
    CriminalPreviewPaneComponent,
    AgePipe,
    AvatarPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ApolloModule,
    HttpLinkModule,
    AppRoutingModule,
    CardModule,
    TableModule,
    PaginatorModule,
    AvatarModule,
    ButtonModule,
    TagModule,
    ConfirmDialogModule,
    BrowserAnimationsModule,
    ToastModule,
    ToolbarModule,
    ProgressSpinnerModule,
    InputTextModule,
    InputNumberModule,
    InputSwitchModule,
    DropdownModule,
    CalendarModule,
    InplaceModule,
    TabMenuModule
  ],
  providers: [
    ConfirmationService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(
    apollo: Apollo,
    httpLink: HttpLink
  ){
    apollo.create({
      link: httpLink.create({uri: 'http://localhost:4000/graphql'}),
      cache: new InMemoryCache()
    })
  }

}
