import { Criminal, CriminalQueryResults } from './types'
import { criminals as allCriminals } from './criminals';

let criminals = allCriminals;

const findCriminals = function({searchTerm, offset, limit}): CriminalQueryResults{
    
    if(limit < 0){
        return {criminals, totalRecords: criminals.length}
    }

    const filteredCriminals = criminals.filter(criminal => {
        const fullname = criminal.first_name + ' ' + criminal.last_name;
        return fullname.toLowerCase().includes(searchTerm.toLowerCase());
    });
    const criminalsSegment = filteredCriminals.slice(offset, offset + limit);
    return {
        criminals: criminalsSegment,
        totalRecords: filteredCriminals.length
    }
}

const getCriminalById = function({id}): Criminal{
    return criminals.find(criminal => criminal.id === id);
}

const deleteCriminalById = function({id}): Criminal{
    const criminalIndex = criminals.findIndex(criminal => criminal.id === id);
    const criminal = criminals[criminalIndex];
    criminals.splice(criminalIndex, 1);
    criminal.deleted = true;
    return criminal;
}

const deleteCriminals = function({criminalIds}): Criminal[]{
    let deletedCriminal: Criminal;
    const deletedCriminals: Criminal[] = [];
    for (const id of criminalIds.ids){
        deletedCriminal = criminals.find(criminal => criminal.id === id);
        deletedCriminal.deleted = true;
        deletedCriminals.push(deletedCriminal);
    }
    criminals = criminals.filter(criminal => !criminalIds.ids.includes(criminal.id));
    return deletedCriminals;
}

const closeContract = function({criminalId}){
    const requestedCriminal = criminals.find(criminal => criminal.id === criminalId);
    requestedCriminal.contract_is_open = false;
    return requestedCriminal;
}

const closeManyContracts = function({criminalIds}): Criminal[]{
    let requestedCriminal: Criminal;
    const requestedCriminals: Criminal[] = [];
    for (const id of criminalIds.ids){
        requestedCriminal = criminals.find(criminal => criminal.id === id);
        requestedCriminal.contract_is_open = false;
        requestedCriminals.push(requestedCriminal);
    }
    return requestedCriminals;
}

const addressProperties = [
    'city',
    'street_name',
    'street_address',
    'zip_code',
    'state',
    'country'
]

const updateCriminal = function({id, updates}): Criminal{
    const requestedCriminal = criminals.find(criminal => criminal.id === id);
    for (const property in updates){
        if(addressProperties.includes(property)){
            requestedCriminal.address[property] = updates[property];
        } else {
            requestedCriminal[property] = updates[property];
        }
    }
    return requestedCriminal;
}

export const root = {
    criminals: findCriminals,
    criminal: getCriminalById,
    closeContract,
    closeManyContracts,
    updateCriminal,
    deleteCriminal: deleteCriminalById,
    deleteCriminals
};