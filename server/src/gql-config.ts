import { schema } from './schema';
import { root } from './root';

export const config = {
    schema,
    rootValue: root,
    graphiql: true
};