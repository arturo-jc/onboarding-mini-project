import express from 'express';
import { graphqlHTTP } from 'express-graphql';
import cors from 'cors';
import { config } from './gql-config';

const app = express();
app.use(cors({
    origin: '*'
}))
app.use('/graphql', graphqlHTTP(config));

const port = 4000;
const msg = `Express GraphQL server now running on localhost:${port}/graphql`

app.listen(port, () => console.log(msg))