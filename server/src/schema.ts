import { buildSchema } from "graphql";

export const schema = buildSchema(`
type Query {
    criminals(searchTerm: String, offset: Int, limit: Int): CriminalQueryResults
    criminal(id: Int!): Criminal
}

type Mutation{
    closeContract(criminalId: Int!): Criminal
    closeManyContracts(criminalIds: criminalIds): [Criminal]
    updateCriminal(id: Int!, updates: Update): Criminal
    deleteCriminal(id: Int!): Criminal
    deleteCriminals(criminalIds: criminalIds): [Criminal]
}

type Criminal {
    id: Int
    first_name: String
    last_name: String
    date_of_birth: Int
    gender: String
    occupation: String
    email: String
    address: Address
    avatar: String
    reward: Int
    contract_is_open: Boolean
    must_return_alive: Boolean
    deleted: Boolean
}

type Address {
    city: String
    street_name: String
    street_address: String
    zip_code: String
    state: String
    country: String
    coordinates: Coordinates
}

type Coordinates {
    lat: Float
    lng: Float
}

type CriminalQueryResults {
    criminals: [Criminal]
    totalRecords: Int
}

input Update {
    id: Int
    avatar: String
    first_name: String
    last_name: String
    date_of_birth: Int
    gender: String
    occupation: String
    email: String
    reward: Int
    contract_is_open: Boolean
    must_return_alive: Boolean
    city: String
    street_name: String
    street_address: String
    zip_code: String
    state: String
    country: String
}

input criminalIds {
    ids: [Int]
}

`);