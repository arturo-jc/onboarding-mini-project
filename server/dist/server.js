"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_graphql_1 = require("express-graphql");
const cors_1 = __importDefault(require("cors"));
const gql_config_1 = require("./gql-config");
const app = (0, express_1.default)();
app.use((0, cors_1.default)({
    origin: '*'
}));
app.use('/graphql', (0, express_graphql_1.graphqlHTTP)(gql_config_1.config));
const port = 4000;
const msg = `Express GraphQL server now running on localhost:${port}/graphql`;
app.listen(port, () => console.log(msg));
