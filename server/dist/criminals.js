"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.criminals = void 0;
const rawCriminals = [
    {
        id: 3573,
        uid: "7bf96347-d5e0-43b7-bbe6-4c732a6aa030",
        password: "9A0ngdCTRq",
        first_name: "Connie",
        last_name: "Kuhn",
        username: "connie.kuhn",
        email: "connie.kuhn@email.com",
        avatar: "https://robohash.org/veletvoluptate.png?size=300x300&set=set1",
        gender: "Genderqueer",
        phone_number: "+1-284 765-636-9257 x6702",
        social_insurance_number: "392594891",
        date_of_birth: "2000-02-18",
        employment: {
            title: "Manufacturing Director",
            key_skill: "Networking skills"
        },
        address: {
            city: "Port Magnolia",
            street_name: "Dach Inlet",
            street_address: "96111 Salina Vista",
            zip_code: "19403-7294",
            state: "Kansas",
            country: "United States",
            coordinates: {
                lat: 61.76717831298808,
                lng: -98.06843551198305
            }
        },
        credit_card: {
            cc_number: "6771-8976-0986-9998"
        },
        subscription: {
            plan: "Silver",
            status: "Blocked",
            payment_method: "Money transfer",
            term: "Full subscription"
        }
    },
    {
        id: 6023,
        uid: "3945a90a-7f5f-4470-b73f-538f926b07f9",
        password: "tSpZKjbdaW",
        first_name: "Chet",
        last_name: "Ullrich",
        username: "chet.ullrich",
        email: "chet.ullrich@email.com",
        avatar: "https://robohash.org/absunteius.png?size=300x300&set=set1",
        gender: "Agender",
        phone_number: "+670 (370) 365-4333 x946",
        social_insurance_number: "131871832",
        date_of_birth: "1992-06-06",
        employment: {
            title: "Technology Facilitator",
            key_skill: "Organisation"
        },
        address: {
            city: "D'Amoreland",
            street_name: "Alina Rest",
            street_address: "7834 Xavier Ridges",
            zip_code: "03170",
            state: "Nebraska",
            country: "United States",
            coordinates: {
                lat: -0.2660923586976338,
                lng: -0.4066252567095887
            }
        },
        credit_card: {
            cc_number: "6771-8975-3623-2963"
        },
        subscription: {
            plan: "Business",
            status: "Idle",
            payment_method: "Debit card",
            term: "Payment in advance"
        }
    },
    {
        id: 4130,
        uid: "66c26ee6-f0bf-4ef0-9b29-bfd1216d0d9a",
        password: "fYMKoJIQrv",
        first_name: "Stan",
        last_name: "Denesik",
        username: "stan.denesik",
        email: "stan.denesik@email.com",
        avatar: "https://robohash.org/rerumdolorqui.png?size=300x300&set=set1",
        gender: "Agender",
        phone_number: "+1-284 739-546-0176 x4012",
        social_insurance_number: "685623688",
        date_of_birth: "1958-08-01",
        employment: {
            title: "Senior Producer",
            key_skill: "Problem solving"
        },
        address: {
            city: "Robelbury",
            street_name: "Sharolyn Islands",
            street_address: "78970 Rosario Passage",
            zip_code: "67570",
            state: "Nevada",
            country: "United States",
            coordinates: {
                lat: -29.445161784947274,
                lng: 119.68563335970373
            }
        },
        credit_card: {
            cc_number: "4598-8288-2383-4100"
        },
        subscription: {
            plan: "Student",
            status: "Pending",
            payment_method: "Cash",
            term: "Annual"
        }
    },
    {
        id: 3183,
        uid: "30517909-a491-4f24-80b4-3ea67af3ecd2",
        password: "fDixtK9I5c",
        first_name: "Emanuel",
        last_name: "Hessel",
        username: "emanuel.hessel",
        email: "emanuel.hessel@email.com",
        avatar: "https://robohash.org/facereconsequunturqui.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+251 388.244.2271 x189",
        social_insurance_number: "194053765",
        date_of_birth: "2000-10-05",
        employment: {
            title: "Human Consulting Specialist",
            key_skill: "Self-motivated"
        },
        address: {
            city: "West Flossie",
            street_name: "Satterfield Mission",
            street_address: "4242 Moore Causeway",
            zip_code: "06436",
            state: "Pennsylvania",
            country: "United States",
            coordinates: {
                lat: -2.7053177432098607,
                lng: 104.42918648422835
            }
        },
        credit_card: {
            cc_number: "4689546126646"
        },
        subscription: {
            plan: "Bronze",
            status: "Active",
            payment_method: "Cheque",
            term: "Payment in advance"
        }
    },
    {
        id: 1558,
        uid: "992252da-7803-4cf7-8afc-64e429810c81",
        password: "VXUCgFWn5x",
        first_name: "Chantell",
        last_name: "Johnston",
        username: "chantell.johnston",
        email: "chantell.johnston@email.com",
        avatar: "https://robohash.org/seddoloresaut.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+357 287.349.7330 x3498",
        social_insurance_number: "749748687",
        date_of_birth: "1987-03-27",
        employment: {
            title: "Customer Advertising Analyst",
            key_skill: "Confidence"
        },
        address: {
            city: "Port Cesarborough",
            street_name: "Yost Square",
            street_address: "741 Jacobs Station",
            zip_code: "76421",
            state: "Maryland",
            country: "United States",
            coordinates: {
                lat: -35.76243323453947,
                lng: 6.734737535417253
            }
        },
        credit_card: {
            cc_number: "6771-8961-7771-5575"
        },
        subscription: {
            plan: "Gold",
            status: "Pending",
            payment_method: "Google Pay",
            term: "Payment in advance"
        }
    },
    {
        id: 9085,
        uid: "dc3415cd-5cd1-4be3-a7c2-cb1768236753",
        password: "cbYLxXRhM2",
        first_name: "Thomasine",
        last_name: "Jacobson",
        username: "thomasine.jacobson",
        email: "thomasine.jacobson@email.com",
        avatar: "https://robohash.org/odioquosmagnam.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+60 (669) 143-9668 x30723",
        social_insurance_number: "169351483",
        date_of_birth: "1998-01-02",
        employment: {
            title: "Advertising Agent",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Lehnerfurt",
            street_name: "Hessel Estate",
            street_address: "354 Jast Court",
            zip_code: "14106",
            state: "Vermont",
            country: "United States",
            coordinates: {
                lat: -67.07029723301608,
                lng: -120.61670688082492
            }
        },
        credit_card: {
            cc_number: "5538-8421-2264-2338"
        },
        subscription: {
            plan: "Platinum",
            status: "Active",
            payment_method: "Money transfer",
            term: "Monthly"
        }
    },
    {
        id: 4959,
        uid: "e18659e2-12a3-4743-8bbf-4d698b7de3c2",
        password: "DUy9NetTPn",
        first_name: "Margarete",
        last_name: "Corwin",
        username: "margarete.corwin",
        email: "margarete.corwin@email.com",
        avatar: "https://robohash.org/aperiamdoloremtempora.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+213 (318) 345-6220 x78264",
        social_insurance_number: "415823954",
        date_of_birth: "1982-07-25",
        employment: {
            title: "Dynamic Strategist",
            key_skill: "Networking skills"
        },
        address: {
            city: "North Quinton",
            street_name: "Hudson Well",
            street_address: "78200 Albertine Island",
            zip_code: "33149",
            state: "Tennessee",
            country: "United States",
            coordinates: {
                lat: 73.73366316928494,
                lng: -69.31089646116749
            }
        },
        credit_card: {
            cc_number: "4602120088264"
        },
        subscription: {
            plan: "Essential",
            status: "Pending",
            payment_method: "Apple Pay",
            term: "Monthly"
        }
    },
    {
        id: 2044,
        uid: "88a8fe30-e2a2-43d5-938d-385b3209e845",
        password: "qC2GDiXvMp",
        first_name: "Hollie",
        last_name: "Rosenbaum",
        username: "hollie.rosenbaum",
        email: "hollie.rosenbaum@email.com",
        avatar: "https://robohash.org/erroreaaliquam.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+372 530-634-6785 x1692",
        social_insurance_number: "746352012",
        date_of_birth: "1976-07-15",
        employment: {
            title: "Advertising Planner",
            key_skill: "Problem solving"
        },
        address: {
            city: "Langoshborough",
            street_name: "Otilia Unions",
            street_address: "2765 Stehr Neck",
            zip_code: "34907-4892",
            state: "West Virginia",
            country: "United States",
            coordinates: {
                lat: -0.8229605725729385,
                lng: -65.48101242185359
            }
        },
        credit_card: {
            cc_number: "4596566917293"
        },
        subscription: {
            plan: "Platinum",
            status: "Blocked",
            payment_method: "Visa checkout",
            term: "Full subscription"
        }
    },
    {
        id: 2270,
        uid: "b779583a-cf0e-4211-888f-6dd0d68bf82c",
        password: "H18tdrZcTD",
        first_name: "Colin",
        last_name: "Trantow",
        username: "colin.trantow",
        email: "colin.trantow@email.com",
        avatar: "https://robohash.org/quisquamsintdistinctio.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+372 568-661-1858",
        social_insurance_number: "767810930",
        date_of_birth: "1975-08-26",
        employment: {
            title: "District Manager",
            key_skill: "Organisation"
        },
        address: {
            city: "Franciscohaven",
            street_name: "Leatha Crescent",
            street_address: "170 Perry Mews",
            zip_code: "02207-4943",
            state: "Virginia",
            country: "United States",
            coordinates: {
                lat: -4.871712872450871,
                lng: -154.3190141537992
            }
        },
        credit_card: {
            cc_number: "4949-9601-7879-6760"
        },
        subscription: {
            plan: "Silver",
            status: "Idle",
            payment_method: "Credit card",
            term: "Annual"
        }
    },
    {
        id: 7173,
        uid: "bcebe8fe-4936-4102-ae79-7d9e3e2ecab3",
        password: "0BxALh7iE8",
        first_name: "Irwin",
        last_name: "Johnson",
        username: "irwin.johnson",
        email: "irwin.johnson@email.com",
        avatar: "https://robohash.org/praesentiumhicrerum.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+82 891.070.5416",
        social_insurance_number: "460174857",
        date_of_birth: "1980-11-10",
        employment: {
            title: "Consulting Designer",
            key_skill: "Proactive"
        },
        address: {
            city: "Lake Johnnyshire",
            street_name: "Becker Walk",
            street_address: "95530 Crona Fort",
            zip_code: "16676",
            state: "North Carolina",
            country: "United States",
            coordinates: {
                lat: 38.257360089932575,
                lng: -40.545061615985475
            }
        },
        credit_card: {
            cc_number: "5277-6646-4021-6337"
        },
        subscription: {
            plan: "Business",
            status: "Pending",
            payment_method: "Cheque",
            term: "Monthly"
        }
    },
    {
        id: 8080,
        uid: "d4ba840e-c772-4abe-9eac-fd9f0565db7e",
        password: "7KgAetrcIN",
        first_name: "Shelton",
        last_name: "Bogisich",
        username: "shelton.bogisich",
        email: "shelton.bogisich@email.com",
        avatar: "https://robohash.org/corporismagnamveritatis.png?size=300x300&set=set1",
        gender: "Genderqueer",
        phone_number: "+223 414.440.2196",
        social_insurance_number: "502261738",
        date_of_birth: "1961-02-17",
        employment: {
            title: "Dynamic Education Representative",
            key_skill: "Leadership"
        },
        address: {
            city: "West Demetriusview",
            street_name: "Kilback Bypass",
            street_address: "132 Latrisha Mountain",
            zip_code: "06927-4916",
            state: "Texas",
            country: "United States",
            coordinates: {
                lat: -80.20209523491562,
                lng: -159.99453057406996
            }
        },
        credit_card: {
            cc_number: "6771-8963-6250-6102"
        },
        subscription: {
            plan: "Free Trial",
            status: "Blocked",
            payment_method: "Bitcoins",
            term: "Full subscription"
        }
    },
    {
        id: 9247,
        uid: "45fdee72-541e-44d9-83c4-e7f9ee83a311",
        password: "DUJh3soELZ",
        first_name: "Josue",
        last_name: "Rosenbaum",
        username: "josue.rosenbaum",
        email: "josue.rosenbaum@email.com",
        avatar: "https://robohash.org/praesentiumvoluptatemquasi.png?size=300x300&set=set1",
        gender: "Polygender",
        phone_number: "+351 745.102.2065 x909",
        social_insurance_number: "214658742",
        date_of_birth: "1962-09-04",
        employment: {
            title: "Hospitality Designer",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Herzogland",
            street_name: "Friesen Trail",
            street_address: "199 Dickinson Locks",
            zip_code: "23598-8324",
            state: "Indiana",
            country: "United States",
            coordinates: {
                lat: 29.511552280650065,
                lng: 71.4211033791942
            }
        },
        credit_card: {
            cc_number: "6771-8982-1969-1814"
        },
        subscription: {
            plan: "Standard",
            status: "Blocked",
            payment_method: "Credit card",
            term: "Monthly"
        }
    },
    {
        id: 7951,
        uid: "2d209085-118a-4e06-878b-f1dd2c3951e5",
        password: "JvwLlkKfEh",
        first_name: "Christian",
        last_name: "Fay",
        username: "christian.fay",
        email: "christian.fay@email.com",
        avatar: "https://robohash.org/saepetotameos.png?size=300x300&set=set1",
        gender: "Male",
        phone_number: "+1-684 (919) 777-0510",
        social_insurance_number: "628740987",
        date_of_birth: "2002-03-05",
        employment: {
            title: "Healthcare Consultant",
            key_skill: "Technical savvy"
        },
        address: {
            city: "New Nadaland",
            street_name: "Rogahn Bridge",
            street_address: "54414 Enrique Lakes",
            zip_code: "77205",
            state: "Rhode Island",
            country: "United States",
            coordinates: {
                lat: -55.467550570146656,
                lng: -148.54793712025173
            }
        },
        credit_card: {
            cc_number: "4383094841364"
        },
        subscription: {
            plan: "Silver",
            status: "Pending",
            payment_method: "Google Pay",
            term: "Full subscription"
        }
    },
    {
        id: 433,
        uid: "73753594-4e92-4e21-aec5-2a4f84e17c1d",
        password: "6OQpCAhY5y",
        first_name: "Juli",
        last_name: "Schultz",
        username: "juli.schultz",
        email: "juli.schultz@email.com",
        avatar: "https://robohash.org/natusrationedolores.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+356 742.585.5125 x013",
        social_insurance_number: "206556037",
        date_of_birth: "1983-10-31",
        employment: {
            title: "Sales Orchestrator",
            key_skill: "Communication"
        },
        address: {
            city: "Alexisport",
            street_name: "Purdy Parks",
            street_address: "692 Hermiston Row",
            zip_code: "96564",
            state: "Texas",
            country: "United States",
            coordinates: {
                lat: -60.274630308618384,
                lng: -64.32289264160248
            }
        },
        credit_card: {
            cc_number: "6771-8975-2067-8338"
        },
        subscription: {
            plan: "Professional",
            status: "Active",
            payment_method: "Cheque",
            term: "Full subscription"
        }
    },
    {
        id: 5320,
        uid: "745ef7eb-f7fb-4ecd-8d09-8b7577f1322b",
        password: "RLgAuo1m3H",
        first_name: "Roscoe",
        last_name: "Stroman",
        username: "roscoe.stroman",
        email: "roscoe.stroman@email.com",
        avatar: "https://robohash.org/reprehenderiteoscorporis.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+45 (973) 231-2054 x95979",
        social_insurance_number: "739513794",
        date_of_birth: "1962-01-31",
        employment: {
            title: "Sales Designer",
            key_skill: "Communication"
        },
        address: {
            city: "Lake Mariellafurt",
            street_name: "Emmerich Ranch",
            street_address: "850 Nicki Ridge",
            zip_code: "17454-9119",
            state: "Montana",
            country: "United States",
            coordinates: {
                lat: 68.33287651026072,
                lng: 81.37819688113314
            }
        },
        credit_card: {
            cc_number: "6771-8948-4342-4003"
        },
        subscription: {
            plan: "Bronze",
            status: "Idle",
            payment_method: "Debit card",
            term: "Payment in advance"
        }
    },
    {
        id: 3824,
        uid: "520a3c3b-2628-45f3-babb-9c14fafaff96",
        password: "Tlty5JO76L",
        first_name: "Marianna",
        last_name: "Beatty",
        username: "marianna.beatty",
        email: "marianna.beatty@email.com",
        avatar: "https://robohash.org/nisioditsunt.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+687 523-380-7602 x367",
        social_insurance_number: "170667620",
        date_of_birth: "2003-04-19",
        employment: {
            title: "Retail Technician",
            key_skill: "Leadership"
        },
        address: {
            city: "Roderickfurt",
            street_name: "Kristi Pines",
            street_address: "98423 Ledner Turnpike",
            zip_code: "80254-0077",
            state: "Wyoming",
            country: "United States",
            coordinates: {
                lat: -4.561063695442158,
                lng: -33.84592084467366
            }
        },
        credit_card: {
            cc_number: "4940-7045-1907-0140"
        },
        subscription: {
            plan: "Silver",
            status: "Pending",
            payment_method: "Bitcoins",
            term: "Monthly"
        }
    },
    {
        id: 5274,
        uid: "ec3c6ff0-c66b-45b9-a317-72675ff59e1c",
        password: "cNSs6rkbEH",
        first_name: "Joy",
        last_name: "Mohr",
        username: "joy.mohr",
        email: "joy.mohr@email.com",
        avatar: "https://robohash.org/veroearumnecessitatibus.png?size=300x300&set=set1",
        gender: "Agender",
        phone_number: "+960 1-389-136-2702 x748",
        social_insurance_number: "177169356",
        date_of_birth: "2004-04-18",
        employment: {
            title: "Dynamic Designer",
            key_skill: "Communication"
        },
        address: {
            city: "Lake Olliefurt",
            street_name: "Truman Hollow",
            street_address: "326 Wolff Plaza",
            zip_code: "25073-1641",
            state: "Illinois",
            country: "United States",
            coordinates: {
                lat: -86.37095181571826,
                lng: -154.9268171919994
            }
        },
        credit_card: {
            cc_number: "4510674280346"
        },
        subscription: {
            plan: "Diamond",
            status: "Pending",
            payment_method: "WeChat Pay",
            term: "Full subscription"
        }
    },
    {
        id: 6098,
        uid: "eff6727d-72b2-4beb-874b-d371bec112da",
        password: "MsEICcQYNk",
        first_name: "Jordon",
        last_name: "Sipes",
        username: "jordon.sipes",
        email: "jordon.sipes@email.com",
        avatar: "https://robohash.org/aenimest.png?size=300x300&set=set1",
        gender: "Male",
        phone_number: "+380 1-832-563-1067 x56041",
        social_insurance_number: "122824824",
        date_of_birth: "1960-05-20",
        employment: {
            title: "Community-Services Engineer",
            key_skill: "Teamwork"
        },
        address: {
            city: "Darleenchester",
            street_name: "Krishna Stravenue",
            street_address: "3065 Schulist Center",
            zip_code: "16846-8313",
            state: "New Hampshire",
            country: "United States",
            coordinates: {
                lat: -71.91890575460151,
                lng: 33.55025672278589
            }
        },
        credit_card: {
            cc_number: "4157-9209-6075-8995"
        },
        subscription: {
            plan: "Premium",
            status: "Blocked",
            payment_method: "Money transfer",
            term: "Full subscription"
        }
    },
    {
        id: 1773,
        uid: "355d234e-9fcb-42cd-84b2-6749bd854efe",
        password: "ZyTDjaYsfp",
        first_name: "Jorge",
        last_name: "Kautzer",
        username: "jorge.kautzer",
        email: "jorge.kautzer@email.com",
        avatar: "https://robohash.org/beataenullasuscipit.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+599 1-477-591-2998",
        social_insurance_number: "761993401",
        date_of_birth: "1987-08-30",
        employment: {
            title: "Regional Mining Technician",
            key_skill: "Confidence"
        },
        address: {
            city: "Port Verenaland",
            street_name: "Jackie Forest",
            street_address: "78560 Kunde Squares",
            zip_code: "06110",
            state: "Michigan",
            country: "United States",
            coordinates: {
                lat: -82.37528896732768,
                lng: -56.9687640092138
            }
        },
        credit_card: {
            cc_number: "5269-2819-4112-2583"
        },
        subscription: {
            plan: "Starter",
            status: "Active",
            payment_method: "Alipay",
            term: "Full subscription"
        }
    },
    {
        id: 4328,
        uid: "90e7beda-ce17-42f4-b504-d5222545ed55",
        password: "tsYbHrJZ7E",
        first_name: "Oren",
        last_name: "Harvey",
        username: "oren.harvey",
        email: "oren.harvey@email.com",
        avatar: "https://robohash.org/odioveliteum.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+61 795-326-4159 x72830",
        social_insurance_number: "109149930",
        date_of_birth: "1997-08-19",
        employment: {
            title: "Construction Strategist",
            key_skill: "Proactive"
        },
        address: {
            city: "Willianfort",
            street_name: "Hickle Underpass",
            street_address: "5992 Bergstrom Circles",
            zip_code: "29311",
            state: "Kansas",
            country: "United States",
            coordinates: {
                lat: 82.86855770582673,
                lng: -142.87216485868322
            }
        },
        credit_card: {
            cc_number: "5234-6432-0751-7347"
        },
        subscription: {
            plan: "Essential",
            status: "Blocked",
            payment_method: "Cash",
            term: "Monthly"
        }
    },
    {
        id: 8691,
        uid: "15e626e6-2b6d-4f90-99ef-5b38eed392f2",
        password: "RTtDFhe4zH",
        first_name: "Abbey",
        last_name: "Powlowski",
        username: "abbey.powlowski",
        email: "abbey.powlowski@email.com",
        avatar: "https://robohash.org/reiciendisdoloressunt.png?size=300x300&set=set1",
        gender: "Polygender",
        phone_number: "+380 (553) 758-0910 x921",
        social_insurance_number: "672617065",
        date_of_birth: "1960-11-12",
        employment: {
            title: "Human Analyst",
            key_skill: "Confidence"
        },
        address: {
            city: "O'Haraburgh",
            street_name: "Alejandro Village",
            street_address: "40819 Grant Groves",
            zip_code: "40399",
            state: "New Jersey",
            country: "United States",
            coordinates: {
                lat: -31.35297797722928,
                lng: -27.378221734736883
            }
        },
        credit_card: {
            cc_number: "5574-3984-6587-2904"
        },
        subscription: {
            plan: "Silver",
            status: "Pending",
            payment_method: "Money transfer",
            term: "Annual"
        }
    },
    {
        id: 932,
        uid: "a6257b72-7ce0-4855-94c3-2bf9b8d9127d",
        password: "xaCgLQK5Vc",
        first_name: "Wyatt",
        last_name: "Champlin",
        username: "wyatt.champlin",
        email: "wyatt.champlin@email.com",
        avatar: "https://robohash.org/autdeseruntautem.png?size=300x300&set=set1",
        gender: "Male",
        phone_number: "+1-340 120-539-9935 x976",
        social_insurance_number: "535746960",
        date_of_birth: "2000-05-08",
        employment: {
            title: "Mining Executive",
            key_skill: "Leadership"
        },
        address: {
            city: "Port Milagro",
            street_name: "Blanca Mall",
            street_address: "706 Royce Center",
            zip_code: "56855-2705",
            state: "Maryland",
            country: "United States",
            coordinates: {
                lat: 38.53314830612314,
                lng: 123.54423073451204
            }
        },
        credit_card: {
            cc_number: "6771-8990-6930-3278"
        },
        subscription: {
            plan: "Bronze",
            status: "Idle",
            payment_method: "Cheque",
            term: "Annual"
        }
    },
    {
        id: 9349,
        uid: "a3c1aaeb-4b63-4aa4-8398-8790882aad0e",
        password: "94JTq1h3RD",
        first_name: "Federico",
        last_name: "Weimann",
        username: "federico.weimann",
        email: "federico.weimann@email.com",
        avatar: "https://robohash.org/occaecatitenetursint.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+852 1-555-351-8348 x1832",
        social_insurance_number: "364210088",
        date_of_birth: "1993-04-03",
        employment: {
            title: "Marketing Supervisor",
            key_skill: "Networking skills"
        },
        address: {
            city: "Kulasport",
            street_name: "Gleichner Walks",
            street_address: "7794 Kiehn Summit",
            zip_code: "60015-4775",
            state: "Tennessee",
            country: "United States",
            coordinates: {
                lat: -72.50457847603816,
                lng: 86.52713871761756
            }
        },
        credit_card: {
            cc_number: "4525-1445-0745-2559"
        },
        subscription: {
            plan: "Starter",
            status: "Blocked",
            payment_method: "Money transfer",
            term: "Annual"
        }
    },
    {
        id: 2174,
        uid: "340db2f3-41bd-4ae3-98bd-19f0aef8804b",
        password: "syqhpd7IjD",
        first_name: "Ira",
        last_name: "Schinner",
        username: "ira.schinner",
        email: "ira.schinner@email.com",
        avatar: "https://robohash.org/sequiquiaut.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+357 1-178-641-8019 x0923",
        social_insurance_number: "353438732",
        date_of_birth: "1990-11-04",
        employment: {
            title: "Legacy Community-Services Officer",
            key_skill: "Leadership"
        },
        address: {
            city: "Port Dorabury",
            street_name: "Christie Street",
            street_address: "2660 Koelpin Bypass",
            zip_code: "93399-1771",
            state: "California",
            country: "United States",
            coordinates: {
                lat: -34.50235708465492,
                lng: -6.74715014585243
            }
        },
        credit_card: {
            cc_number: "4137-1465-0224-8510"
        },
        subscription: {
            plan: "Platinum",
            status: "Pending",
            payment_method: "Apple Pay",
            term: "Monthly"
        }
    },
    {
        id: 3007,
        uid: "5fe57ac0-3ee3-4994-af14-34559210961a",
        password: "6iSGjtX3b5",
        first_name: "Don",
        last_name: "Torphy",
        username: "don.torphy",
        email: "don.torphy@email.com",
        avatar: "https://robohash.org/utauta.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+62 380-390-9800 x934",
        social_insurance_number: "416617777",
        date_of_birth: "1998-12-05",
        employment: {
            title: "Investor Administration Director",
            key_skill: "Teamwork"
        },
        address: {
            city: "West Terrancetown",
            street_name: "Ricki Pines",
            street_address: "7829 Gaston Common",
            zip_code: "50327-8423",
            state: "Idaho",
            country: "United States",
            coordinates: {
                lat: 65.09259638196278,
                lng: -169.69180877471166
            }
        },
        credit_card: {
            cc_number: "5334-2401-9231-5255"
        },
        subscription: {
            plan: "Gold",
            status: "Pending",
            payment_method: "Paypal",
            term: "Monthly"
        }
    },
    {
        id: 5495,
        uid: "0804d5fe-fcca-4b2a-b3de-985cc5fef4fa",
        password: "9qMzjLwUk1",
        first_name: "Ronnie",
        last_name: "Raynor",
        username: "ronnie.raynor",
        email: "ronnie.raynor@email.com",
        avatar: "https://robohash.org/quasivoluptasautem.png?size=300x300&set=set1",
        gender: "Agender",
        phone_number: "+1-939 603-713-7433 x108",
        social_insurance_number: "764649521",
        date_of_birth: "1978-10-09",
        employment: {
            title: "Global Technology Designer",
            key_skill: "Proactive"
        },
        address: {
            city: "Forrestshire",
            street_name: "Schmeler Club",
            street_address: "2041 Paucek Village",
            zip_code: "05564-1639",
            state: "Ohio",
            country: "United States",
            coordinates: {
                lat: -49.864998544326816,
                lng: 146.6896401661619
            }
        },
        credit_card: {
            cc_number: "6771-8940-6368-7255"
        },
        subscription: {
            plan: "Bronze",
            status: "Pending",
            payment_method: "Visa checkout",
            term: "Full subscription"
        }
    },
    {
        id: 4292,
        uid: "607889c1-8fe2-4240-91f1-0379279b7177",
        password: "OxMHnlNJTa",
        first_name: "Clayton",
        last_name: "Gislason",
        username: "clayton.gislason",
        email: "clayton.gislason@email.com",
        avatar: "https://robohash.org/expeditaquidicta.png?size=300x300&set=set1",
        gender: "Male",
        phone_number: "+677 291-734-8125 x4005",
        social_insurance_number: "521904946",
        date_of_birth: "1978-06-16",
        employment: {
            title: "Legacy Advertising Planner",
            key_skill: "Work under pressure"
        },
        address: {
            city: "Altheashire",
            street_name: "Donnette Extensions",
            street_address: "68391 Alec Crossroad",
            zip_code: "72603",
            state: "California",
            country: "United States",
            coordinates: {
                lat: 25.957804900209908,
                lng: 43.983758443371926
            }
        },
        credit_card: {
            cc_number: "4583051009513"
        },
        subscription: {
            plan: "Premium",
            status: "Idle",
            payment_method: "Cheque",
            term: "Payment in advance"
        }
    },
    {
        id: 4691,
        uid: "685cb78b-8a66-4387-94df-6aa7a64682cd",
        password: "YBD5a2NqGv",
        first_name: "Rana",
        last_name: "Kassulke",
        username: "rana.kassulke",
        email: "rana.kassulke@email.com",
        avatar: "https://robohash.org/blanditiiseumut.png?size=300x300&set=set1",
        gender: "Genderqueer",
        phone_number: "+267 (420) 990-4633",
        social_insurance_number: "642003313",
        date_of_birth: "1994-02-21",
        employment: {
            title: "Design Liaison",
            key_skill: "Self-motivated"
        },
        address: {
            city: "New Lajuanastad",
            street_name: "Kevin Locks",
            street_address: "919 Tim Ranch",
            zip_code: "77517",
            state: "North Carolina",
            country: "United States",
            coordinates: {
                lat: -40.87119831701156,
                lng: 143.32415376263236
            }
        },
        credit_card: {
            cc_number: "4467207030013"
        },
        subscription: {
            plan: "Professional",
            status: "Pending",
            payment_method: "Bitcoins",
            term: "Monthly"
        }
    },
    {
        id: 624,
        uid: "ae4847a0-86e3-49fe-9bbd-c9e922529372",
        password: "D2fMKwEG9g",
        first_name: "Edison",
        last_name: "Smith",
        username: "edison.smith",
        email: "edison.smith@email.com",
        avatar: "https://robohash.org/molestiaeliberoreprehenderit.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+64 (134) 007-3532 x4486",
        social_insurance_number: "415616689",
        date_of_birth: "1977-01-07",
        employment: {
            title: "Central Technology Assistant",
            key_skill: "Self-motivated"
        },
        address: {
            city: "South Katelynnstad",
            street_name: "Shields Burg",
            street_address: "447 Dario Rapid",
            zip_code: "01074-6200",
            state: "Oregon",
            country: "United States",
            coordinates: {
                lat: -11.43765600980187,
                lng: -102.14770806142765
            }
        },
        credit_card: {
            cc_number: "4237-0428-4739-3187"
        },
        subscription: {
            plan: "Professional",
            status: "Idle",
            payment_method: "WeChat Pay",
            term: "Payment in advance"
        }
    },
    {
        id: 6252,
        uid: "5d0a0496-69a0-4a91-9f37-fa88b7be1ee5",
        password: "LAIakz1pMW",
        first_name: "Sherley",
        last_name: "Kautzer",
        username: "sherley.kautzer",
        email: "sherley.kautzer@email.com",
        avatar: "https://robohash.org/laudantiummagnamea.png?size=300x300&set=set1",
        gender: "Genderqueer",
        phone_number: "+1-264 (834) 805-6133 x4810",
        social_insurance_number: "102965589",
        date_of_birth: "1986-12-29",
        employment: {
            title: "Hospitality Engineer",
            key_skill: "Self-motivated"
        },
        address: {
            city: "Port Angele",
            street_name: "Earle Land",
            street_address: "7454 Nolan Square",
            zip_code: "71374-2479",
            state: "Arkansas",
            country: "United States",
            coordinates: {
                lat: 2.0185679177609046,
                lng: 72.34645232500054
            }
        },
        credit_card: {
            cc_number: "5238-9551-4660-2220"
        },
        subscription: {
            plan: "Free Trial",
            status: "Idle",
            payment_method: "Cheque",
            term: "Payment in advance"
        }
    },
    {
        id: 8330,
        uid: "abbc8edc-08f8-4ab4-8e40-d9d75a66d994",
        password: "feXBlnmwYM",
        first_name: "Joanie",
        last_name: "Zemlak",
        username: "joanie.zemlak",
        email: "joanie.zemlak@email.com",
        avatar: "https://robohash.org/corruptidistinctiofacilis.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+500 949-436-2253 x0576",
        social_insurance_number: "948978234",
        date_of_birth: "2001-05-24",
        employment: {
            title: "National Officer",
            key_skill: "Teamwork"
        },
        address: {
            city: "Port Patricia",
            street_name: "Trent Brook",
            street_address: "333 Luigi Cove",
            zip_code: "80664",
            state: "Nebraska",
            country: "United States",
            coordinates: {
                lat: 81.33125018819217,
                lng: 22.124660446000092
            }
        },
        credit_card: {
            cc_number: "4322681615815"
        },
        subscription: {
            plan: "Student",
            status: "Active",
            payment_method: "WeChat Pay",
            term: "Full subscription"
        }
    },
    {
        id: 9291,
        uid: "6a5318eb-7448-4159-a83e-bf5cb203b6cd",
        password: "PmNBzJQbIw",
        first_name: "Jimmie",
        last_name: "Cartwright",
        username: "jimmie.cartwright",
        email: "jimmie.cartwright@email.com",
        avatar: "https://robohash.org/dignissimossedmagni.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+265 532.637.7447",
        social_insurance_number: "991841016",
        date_of_birth: "1994-01-03",
        employment: {
            title: "Marketing Designer",
            key_skill: "Fast learner"
        },
        address: {
            city: "South Kera",
            street_name: "Micah Valleys",
            street_address: "52765 Wisozk Fords",
            zip_code: "88979-0866",
            state: "New Mexico",
            country: "United States",
            coordinates: {
                lat: -3.516359803767415,
                lng: -23.563917351087213
            }
        },
        credit_card: {
            cc_number: "5234-8735-3004-9649"
        },
        subscription: {
            plan: "Professional",
            status: "Active",
            payment_method: "Money transfer",
            term: "Payment in advance"
        }
    },
    {
        id: 6811,
        uid: "1e8c85ce-34d5-45f9-94f9-1bede2a175ec",
        password: "Ro4c2ihBrK",
        first_name: "Chara",
        last_name: "Kris",
        username: "chara.kris",
        email: "chara.kris@email.com",
        avatar: "https://robohash.org/optioetsuscipit.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+54 516-719-2318 x7193",
        social_insurance_number: "187235064",
        date_of_birth: "1974-02-28",
        employment: {
            title: "Human Associate",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Metzville",
            street_name: "Loise Turnpike",
            street_address: "52884 Kilback Canyon",
            zip_code: "56531-8980",
            state: "Colorado",
            country: "United States",
            coordinates: {
                lat: -16.801982422902682,
                lng: -115.89204988880553
            }
        },
        credit_card: {
            cc_number: "5116-3581-8957-1021"
        },
        subscription: {
            plan: "Basic",
            status: "Active",
            payment_method: "Google Pay",
            term: "Full subscription"
        }
    },
    {
        id: 7552,
        uid: "33e55edf-4245-4de8-8967-c077a91a8fe5",
        password: "pZyQYtz805",
        first_name: "Carter",
        last_name: "Crooks",
        username: "carter.crooks",
        email: "carter.crooks@email.com",
        avatar: "https://robohash.org/voluptatemfaceremolestiae.png?size=300x300&set=set1",
        gender: "Polygender",
        phone_number: "+248 (979) 298-0609",
        social_insurance_number: "438307837",
        date_of_birth: "1995-05-20",
        employment: {
            title: "Customer Developer",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Cronaborough",
            street_name: "Murray Club",
            street_address: "687 Wolf Flats",
            zip_code: "83514",
            state: "Connecticut",
            country: "United States",
            coordinates: {
                lat: 39.28149926640262,
                lng: 73.65657688883721
            }
        },
        credit_card: {
            cc_number: "4156-3536-2657-7040"
        },
        subscription: {
            plan: "Free Trial",
            status: "Pending",
            payment_method: "Alipay",
            term: "Monthly"
        }
    },
    {
        id: 1572,
        uid: "9ef562a6-e62f-477f-8220-f18ae9a04986",
        password: "JVxRcKC786",
        first_name: "Myra",
        last_name: "Kris",
        username: "myra.kris",
        email: "myra.kris@email.com",
        avatar: "https://robohash.org/omnislaboresit.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+47 774.900.8302 x8090",
        social_insurance_number: "470260738",
        date_of_birth: "1979-09-27",
        employment: {
            title: "Community-Services Executive",
            key_skill: "Fast learner"
        },
        address: {
            city: "Vonborough",
            street_name: "Doloris Overpass",
            street_address: "52773 Goldner Grove",
            zip_code: "27872",
            state: "Utah",
            country: "United States",
            coordinates: {
                lat: -29.876907325408588,
                lng: -73.13808969867493
            }
        },
        credit_card: {
            cc_number: "4785437836044"
        },
        subscription: {
            plan: "Silver",
            status: "Blocked",
            payment_method: "Visa checkout",
            term: "Payment in advance"
        }
    },
    {
        id: 4083,
        uid: "e1a728b2-a8ab-4657-9a36-18a252d0fd0e",
        password: "OIw2U3Z7Jr",
        first_name: "Alan",
        last_name: "Senger",
        username: "alan.senger",
        email: "alan.senger@email.com",
        avatar: "https://robohash.org/etducimusanimi.png?size=300x300&set=set1",
        gender: "Polygender",
        phone_number: "+269 832.285.4811",
        social_insurance_number: "429247018",
        date_of_birth: "2003-01-02",
        employment: {
            title: "Design Engineer",
            key_skill: "Confidence"
        },
        address: {
            city: "East Debroahbury",
            street_name: "Robby Turnpike",
            street_address: "46545 Guillermo Pines",
            zip_code: "51828",
            state: "California",
            country: "United States",
            coordinates: {
                lat: 84.29370430417086,
                lng: 134.16004315391223
            }
        },
        credit_card: {
            cc_number: "5547-7034-8103-6406"
        },
        subscription: {
            plan: "Silver",
            status: "Idle",
            payment_method: "Google Pay",
            term: "Monthly"
        }
    },
    {
        id: 7360,
        uid: "dba90d88-36b2-4186-86e8-2a0c90907464",
        password: "ap7RvgncO5",
        first_name: "Min",
        last_name: "Renner",
        username: "min.renner",
        email: "min.renner@email.com",
        avatar: "https://robohash.org/sequiblanditiisconsectetur.png?size=300x300&set=set1",
        gender: "Male",
        phone_number: "+20 (540) 942-5498 x688",
        social_insurance_number: "610875981",
        date_of_birth: "1978-02-13",
        employment: {
            title: "Regional Legal Director",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Gerholdchester",
            street_name: "Stamm Union",
            street_address: "409 Nancy Creek",
            zip_code: "40950",
            state: "Mississippi",
            country: "United States",
            coordinates: {
                lat: -73.9822551999825,
                lng: 9.82308304012102
            }
        },
        credit_card: {
            cc_number: "4511320500467"
        },
        subscription: {
            plan: "Starter",
            status: "Idle",
            payment_method: "Credit card",
            term: "Monthly"
        }
    },
    {
        id: 8906,
        uid: "ed407840-2879-4f2b-bc4b-68031076110c",
        password: "SosPQ6TykC",
        first_name: "Darryl",
        last_name: "Rosenbaum",
        username: "darryl.rosenbaum",
        email: "darryl.rosenbaum@email.com",
        avatar: "https://robohash.org/corporisadipiscidolore.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+968 1-481-457-5256 x4741",
        social_insurance_number: "197379860",
        date_of_birth: "2002-04-10",
        employment: {
            title: "Administration Liaison",
            key_skill: "Organisation"
        },
        address: {
            city: "Lake Delmerville",
            street_name: "Bridget Course",
            street_address: "5562 Kuhic Walk",
            zip_code: "63376-9617",
            state: "Kansas",
            country: "United States",
            coordinates: {
                lat: 84.60618832529906,
                lng: 55.635972448325504
            }
        },
        credit_card: {
            cc_number: "5589-1377-0189-9914"
        },
        subscription: {
            plan: "Silver",
            status: "Idle",
            payment_method: "Paypal",
            term: "Annual"
        }
    },
    {
        id: 8927,
        uid: "84feffd8-c74c-4308-affe-e72d3e236b24",
        password: "kLvGMVXnmp",
        first_name: "Edwardo",
        last_name: "Gleichner",
        username: "edwardo.gleichner",
        email: "edwardo.gleichner@email.com",
        avatar: "https://robohash.org/suntmodiid.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+961 (822) 144-2169 x2724",
        social_insurance_number: "260125877",
        date_of_birth: "1981-10-08",
        employment: {
            title: "Internal Planner",
            key_skill: "Proactive"
        },
        address: {
            city: "West Cristine",
            street_name: "Kathryne Ranch",
            street_address: "403 Jacobs Village",
            zip_code: "28620-8743",
            state: "Florida",
            country: "United States",
            coordinates: {
                lat: 86.82751194624626,
                lng: 67.85200678863524
            }
        },
        credit_card: {
            cc_number: "4496-4866-8145-5204"
        },
        subscription: {
            plan: "Silver",
            status: "Pending",
            payment_method: "Google Pay",
            term: "Monthly"
        }
    },
    {
        id: 6517,
        uid: "0a061678-3830-4594-8192-837c62015e5a",
        password: "ynFCxURjTu",
        first_name: "Quintin",
        last_name: "Ruecker",
        username: "quintin.ruecker",
        email: "quintin.ruecker@email.com",
        avatar: "https://robohash.org/omnisquaeratvero.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+1-242 1-462-508-3464 x4306",
        social_insurance_number: "267372209",
        date_of_birth: "1985-04-25",
        employment: {
            title: "Marketing Assistant",
            key_skill: "Organisation"
        },
        address: {
            city: "Port Israelville",
            street_name: "Robbie Hills",
            street_address: "5516 Clement Junctions",
            zip_code: "24897",
            state: "Colorado",
            country: "United States",
            coordinates: {
                lat: -43.02195551366012,
                lng: 149.03400599950646
            }
        },
        credit_card: {
            cc_number: "4726087043309"
        },
        subscription: {
            plan: "Diamond",
            status: "Blocked",
            payment_method: "Paypal",
            term: "Annual"
        }
    },
    {
        id: 8561,
        uid: "1a824c2c-0587-4743-9f4d-4816faf64716",
        password: "nwjz5IGq2X",
        first_name: "Garry",
        last_name: "Halvorson",
        username: "garry.halvorson",
        email: "garry.halvorson@email.com",
        avatar: "https://robohash.org/etdignissimosnesciunt.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+423 441.645.1714 x68114",
        social_insurance_number: "551846272",
        date_of_birth: "1988-12-15",
        employment: {
            title: "Farming Producer",
            key_skill: "Networking skills"
        },
        address: {
            city: "South Devon",
            street_name: "Jamison Walk",
            street_address: "9952 Nolan Lodge",
            zip_code: "71224-1998",
            state: "Tennessee",
            country: "United States",
            coordinates: {
                lat: 27.4916376407583,
                lng: -143.24159667268665
            }
        },
        credit_card: {
            cc_number: "4357-7673-5863-9636"
        },
        subscription: {
            plan: "Diamond",
            status: "Pending",
            payment_method: "Debit card",
            term: "Full subscription"
        }
    },
    {
        id: 9608,
        uid: "04550e88-fc5d-4730-9e49-8b528c645c28",
        password: "xbJ8GLhM4t",
        first_name: "Leonia",
        last_name: "Ratke",
        username: "leonia.ratke",
        email: "leonia.ratke@email.com",
        avatar: "https://robohash.org/officiisvelitnemo.png?size=300x300&set=set1",
        gender: "Genderqueer",
        phone_number: "+61 159.465.5246",
        social_insurance_number: "588036384",
        date_of_birth: "1967-03-20",
        employment: {
            title: "Dynamic Marketing Technician",
            key_skill: "Leadership"
        },
        address: {
            city: "Lake Suzan",
            street_name: "Melvin Loaf",
            street_address: "135 Isaiah Forest",
            zip_code: "36850-9807",
            state: "New York",
            country: "United States",
            coordinates: {
                lat: -36.311482859721636,
                lng: 176.66727402093886
            }
        },
        credit_card: {
            cc_number: "4156060827777"
        },
        subscription: {
            plan: "Standard",
            status: "Pending",
            payment_method: "Alipay",
            term: "Payment in advance"
        }
    },
    {
        id: 6474,
        uid: "90728b1b-f9d6-47ab-95c7-60816acf4c8f",
        password: "LyigFSMrv1",
        first_name: "Dimple",
        last_name: "Maggio",
        username: "dimple.maggio",
        email: "dimple.maggio@email.com",
        avatar: "https://robohash.org/doloredeseruntexplicabo.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+93 417.800.8396 x91015",
        social_insurance_number: "337707533",
        date_of_birth: "1959-02-23",
        employment: {
            title: "Design Producer",
            key_skill: "Technical savvy"
        },
        address: {
            city: "Allegrastad",
            street_name: "Megan Ramp",
            street_address: "4654 King Plains",
            zip_code: "80273",
            state: "Wyoming",
            country: "United States",
            coordinates: {
                lat: -9.35350743947616,
                lng: -31.286659209087446
            }
        },
        credit_card: {
            cc_number: "6771-8964-4470-3800"
        },
        subscription: {
            plan: "Silver",
            status: "Idle",
            payment_method: "Money transfer",
            term: "Annual"
        }
    },
    {
        id: 4215,
        uid: "5d3c2407-a31e-4ab2-800a-17e91f0e4467",
        password: "mCoKFdEhDp",
        first_name: "Lovella",
        last_name: "Willms",
        username: "lovella.willms",
        email: "lovella.willms@email.com",
        avatar: "https://robohash.org/ipsafacerevoluptatem.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+264 1-193-082-8763 x183",
        social_insurance_number: "778104612",
        date_of_birth: "1985-10-21",
        employment: {
            title: "Global Strategist",
            key_skill: "Networking skills"
        },
        address: {
            city: "Porfiriofort",
            street_name: "Marcelino Extension",
            street_address: "26807 Buster Turnpike",
            zip_code: "32133-8036",
            state: "Indiana",
            country: "United States",
            coordinates: {
                lat: 77.40360067107267,
                lng: -4.729520435810684
            }
        },
        credit_card: {
            cc_number: "6771-8948-7243-5102"
        },
        subscription: {
            plan: "Diamond",
            status: "Idle",
            payment_method: "Visa checkout",
            term: "Full subscription"
        }
    },
    {
        id: 185,
        uid: "3ddbc384-0684-46c1-9cbb-9628e15a7740",
        password: "zOnGDl4r57",
        first_name: "Cleveland",
        last_name: "Ernser",
        username: "cleveland.ernser",
        email: "cleveland.ernser@email.com",
        avatar: "https://robohash.org/errorhicnulla.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+61-8 (967) 071-2609 x5319",
        social_insurance_number: "517211603",
        date_of_birth: "1997-10-22",
        employment: {
            title: "Retail Producer",
            key_skill: "Problem solving"
        },
        address: {
            city: "North Laveta",
            street_name: "Sade Plains",
            street_address: "6721 Cyril Crest",
            zip_code: "00820",
            state: "New Hampshire",
            country: "United States",
            coordinates: {
                lat: -25.124855286076325,
                lng: 146.64206594431118
            }
        },
        credit_card: {
            cc_number: "4247-7477-7232-4167"
        },
        subscription: {
            plan: "Standard",
            status: "Active",
            payment_method: "WeChat Pay",
            term: "Annual"
        }
    },
    {
        id: 6363,
        uid: "e8772449-395e-4177-8d2d-c636a5946092",
        password: "MNojPeq5CL",
        first_name: "Sixta",
        last_name: "Pouros",
        username: "sixta.pouros",
        email: "sixta.pouros@email.com",
        avatar: "https://robohash.org/dolorumsitvoluptatem.png?size=300x300&set=set1",
        gender: "Bigender",
        phone_number: "+297 1-444-906-6015 x532",
        social_insurance_number: "936276880",
        date_of_birth: "2003-04-05",
        employment: {
            title: "Internal Construction Representative",
            key_skill: "Confidence"
        },
        address: {
            city: "South Erich",
            street_name: "Rachele Crossroad",
            street_address: "122 Scot Land",
            zip_code: "16810-0498",
            state: "Louisiana",
            country: "United States",
            coordinates: {
                lat: 81.38967325278625,
                lng: -128.7103484376845
            }
        },
        credit_card: {
            cc_number: "4620-4683-8476-4420"
        },
        subscription: {
            plan: "Diamond",
            status: "Blocked",
            payment_method: "Apple Pay",
            term: "Annual"
        }
    },
    {
        id: 1465,
        uid: "72788734-b89b-47de-9fdb-3cd0636555f8",
        password: "XimjnKAyIr",
        first_name: "Earline",
        last_name: "Harber",
        username: "earline.harber",
        email: "earline.harber@email.com",
        avatar: "https://robohash.org/voluptatibusipsamminima.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+30 1-453-157-8197 x4347",
        social_insurance_number: "435424023",
        date_of_birth: "1993-12-20",
        employment: {
            title: "Construction Designer",
            key_skill: "Leadership"
        },
        address: {
            city: "Stanleyfort",
            street_name: "Keebler Hills",
            street_address: "282 Walsh Port",
            zip_code: "08292",
            state: "Florida",
            country: "United States",
            coordinates: {
                lat: 85.7811889321757,
                lng: 127.61998882456254
            }
        },
        credit_card: {
            cc_number: "4252771807370"
        },
        subscription: {
            plan: "Business",
            status: "Pending",
            payment_method: "Cheque",
            term: "Full subscription"
        }
    },
    {
        id: 7712,
        uid: "13971870-4ba6-430d-80a7-8aef0bfe730b",
        password: "bCjN4EYaWo",
        first_name: "Taryn",
        last_name: "Cummings",
        username: "taryn.cummings",
        email: "taryn.cummings@email.com",
        avatar: "https://robohash.org/dolorumexercitationemiusto.png?size=300x300&set=set1",
        gender: "Genderfluid",
        phone_number: "+963 1-936-252-7164 x428",
        social_insurance_number: "601094022",
        date_of_birth: "1972-06-17",
        employment: {
            title: "Advertising Executive",
            key_skill: "Networking skills"
        },
        address: {
            city: "North Jacquetta",
            street_name: "Daugherty Lodge",
            street_address: "31875 Strosin Gardens",
            zip_code: "86661-1971",
            state: "Kentucky",
            country: "United States",
            coordinates: {
                lat: 42.93933086978541,
                lng: 7.496354268991979
            }
        },
        credit_card: {
            cc_number: "6771-8931-6346-8913"
        },
        subscription: {
            plan: "Basic",
            status: "Active",
            payment_method: "Google Pay",
            term: "Annual"
        }
    },
    {
        id: 6370,
        uid: "f36e1267-5fc2-469f-a0af-f6e3aef89aa0",
        password: "ZRgSY0m1eJ",
        first_name: "Taneka",
        last_name: "Renner",
        username: "taneka.renner",
        email: "taneka.renner@email.com",
        avatar: "https://robohash.org/illumullamearum.png?size=300x300&set=set1",
        gender: "Non-binary",
        phone_number: "+686 (500) 584-2319 x77251",
        social_insurance_number: "249229956",
        date_of_birth: "1959-05-30",
        employment: {
            title: "IT Liaison",
            key_skill: "Networking skills"
        },
        address: {
            city: "Port Blossom",
            street_name: "Ai Estates",
            street_address: "874 Mack Common",
            zip_code: "39379",
            state: "Oklahoma",
            country: "United States",
            coordinates: {
                lat: 79.23089903426961,
                lng: 79.48590268125116
            }
        },
        credit_card: {
            cc_number: "6771-8993-1308-4070"
        },
        subscription: {
            plan: "Professional",
            status: "Blocked",
            payment_method: "Bitcoins",
            term: "Annual"
        }
    },
    {
        id: 5831,
        uid: "9fe3c886-ada7-43ae-b86e-27fbc6b9950d",
        password: "GoEmBONHJI",
        first_name: "Lesa",
        last_name: "Grimes",
        username: "lesa.grimes",
        email: "lesa.grimes@email.com",
        avatar: "https://robohash.org/quodutincidunt.png?size=300x300&set=set1",
        gender: "Female",
        phone_number: "+856 146.487.0489",
        social_insurance_number: "580128676",
        date_of_birth: "1999-02-16",
        employment: {
            title: "Government Coordinator",
            key_skill: "Leadership"
        },
        address: {
            city: "Lake Susanne",
            street_name: "Abe Common",
            street_address: "4843 Simonis Cliff",
            zip_code: "36807",
            state: "Massachusetts",
            country: "United States",
            coordinates: {
                lat: 89.51528212528953,
                lng: 15.581018156283704
            }
        },
        credit_card: {
            cc_number: "6771-8995-6161-3620"
        },
        subscription: {
            plan: "Gold",
            status: "Blocked",
            payment_method: "Apple Pay",
            term: "Annual"
        }
    }
];
exports.criminals = rawCriminals.map(criminal => {
    return {
        id: criminal.id,
        first_name: criminal.first_name,
        last_name: criminal.last_name,
        // Convert DOB string to unix timestamp
        date_of_birth: Math.floor(new Date(criminal.date_of_birth).getTime() / 1000),
        gender: criminal.gender,
        occupation: criminal.employment.title,
        email: criminal.email,
        address: criminal.address,
        avatar: criminal.avatar,
        // Assign random reward between 1,000 and 10,000 
        reward: Math.max(1000 + Math.round(Math.floor(Math.random() * 10) * 1000)),
        contract_is_open: true,
        must_return_alive: Math.random() > 0.5,
        deleted: false
    };
});
