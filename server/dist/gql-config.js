"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const schema_1 = require("./schema");
const root_1 = require("./root");
exports.config = {
    schema: schema_1.schema,
    rootValue: root_1.root,
    graphiql: true
};
